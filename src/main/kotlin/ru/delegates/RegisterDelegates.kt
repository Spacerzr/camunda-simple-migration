package ru.delegates

import org.camunda.bpm.engine.delegate.DelegateExecution
import org.camunda.bpm.engine.delegate.JavaDelegate
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class ApproveAccountDelegate @Autowired constructor() : JavaDelegate {
    companion object {
        private val log = LoggerFactory.getLogger(ApproveAccountDelegate::class.java)
    }

    @Throws(Exception::class)
    override fun execute(execution: DelegateExecution) {
        log.info("Send APPROVE email to user with id = {}", execution.businessKey)
    }
}

@Component
class DeclineAccountDelegate @Autowired constructor() : JavaDelegate {
    companion object {
        private val log = LoggerFactory.getLogger(DeclineAccountDelegate::class.java)
    }

    @Throws(java.lang.Exception::class)
    override fun execute(execution: DelegateExecution) {
        log.info("Send DECLINE email to user with id = {}", execution.businessKey)
    }
}