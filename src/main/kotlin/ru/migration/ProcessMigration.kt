package ru.migration

import org.camunda.bpm.engine.RepositoryService
import org.camunda.bpm.engine.RuntimeService
import org.camunda.bpm.engine.repository.ProcessDefinition
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component


@Component
class ProcessMigration @Autowired constructor(private val runtimeService: RuntimeService,
                                              private val repositoryService: RepositoryService) {
    companion object {
        private val log = LoggerFactory.getLogger(ProcessMigration::class.java)
        const val processDefinitionKey = "registration_user_process"
    }

    @EventListener
    fun onApplicationReady(e: ApplicationReadyEvent) {
        log.info("Checking if migration are needed for process definition key: {}", processDefinitionKey)

        val allProcessDefinitions = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey(processDefinitionKey).list()
        if (allProcessDefinitions.size > 1) {
            log.info("Migration are required. Identify instances")

            val latestProcessDefinition = repositoryService.createProcessDefinitionQuery()
                    .processDefinitionKey(processDefinitionKey).latestVersion().singleResult()
            val oldProcessDefinitions = allProcessDefinitions.filter { it.id != latestProcessDefinition.id }

            migrate(oldProcessDefinitions, latestProcessDefinition)
            removeUnusedDeployments(oldProcessDefinitions)
        } else {
            log.info("All process instances are in the latest version of process definition. Migration are not required.")
        }
    }

    private fun migrate(sourceProcessDefinitions: List<ProcessDefinition>, targetDefinition: ProcessDefinition) {
        log.info("Start migration")

        val preparedMigrations = sourceProcessDefinitions
                .map {
                    val processInstances = runtimeService.createProcessInstanceQuery()
                            .processDefinitionId(it.id)
                            .active()
                            .list()
                    Pair(it, processInstances)
                }
                .filter { it.second.isNotEmpty() }
                .map {
                    val migrationPlan = runtimeService
                            .createMigrationPlan(it.first.id, targetDefinition.id)
                            .mapEqualActivities()
                            .build()
                    runtimeService.newMigration(migrationPlan)
                            .processInstanceIds(it.second.map { instance -> instance.id })
                }
        preparedMigrations.forEach {
            try {
                it.execute()
            } catch (ex: Exception) {
                log.error("Error migration")
            }
        }
        log.info("Migration complete")
    }

    private fun removeUnusedDeployments(oldProcessDefinitions: List<ProcessDefinition>) {
        val deploymentsToRemove = repositoryService.createDeploymentQuery().list()
                .filter { oldProcessDefinitions.map { definition -> definition.deploymentId }.contains(it.id) }
        deploymentsToRemove.forEach {
            val definitionsToRemoveIds = repositoryService
                    .createProcessDefinitionQuery()
                    .deploymentId(it.id).list().map { processDefinition ->  processDefinition.id }
            repositoryService.deleteDeployment(it.id)
            log.info("Delete deployment: {}; process definitions: {}", it.id, definitionsToRemoveIds)
        }
    }
}