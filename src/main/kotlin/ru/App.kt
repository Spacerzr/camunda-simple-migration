package ru

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.context.properties.ConfigurationPropertiesScan

@SpringBootApplication
@EnableProcessApplication("camunda-migration")
@ConfigurationPropertiesScan
class App

fun main(args: Array<String>) {
    SpringApplicationBuilder(App::class.java)
            .main(App::class.java)
            .run(*args)
}